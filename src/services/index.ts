import { jwtDecode } from "jwt-decode";


interface GNS3aaSJwtPayload {
  SuperUser: number;
  User: number;
}

class AuthService {


  logout() {
    localStorage.removeItem('token');
  }

  getToken() {
    return localStorage.getItem('token');
  }

  setToken(token: any) {
    localStorage.setItem('token', token);
  }

  getUserGroup() {
    const token:any = this.getToken();
    const decoded = jwtDecode(token) as GNS3aaSJwtPayload;
    if (decoded?.SuperUser == 1){
      return 'SuperUser';
    } else if (decoded?.User == 1){
      return 'User';
    } else {
      return 'None';
    }
  }

  isAuthenticated(): boolean {
    const token = this.getToken();
    if (token === "undefined"){
      return false;
    }

    return Boolean(token && !this.isTokenExpired(token));
  }

  isTokenExpired(token: any) {
    const decoded = jwtDecode(token);
    return decoded?.exp && decoded.exp < Date.now() / 1000;
  }
}

export default new AuthService();
