import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import PostLogin from '../views/PostLogin.vue'
import Dashboard from '../views/Dashboard.vue'
import AuthService from '../services'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard,
      //meta: { requiresAuth: true }
    },
    {
      path: '/postLogin',
      name: 'postLogin',
      component: PostLogin
    },
  ]
})

router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth && !AuthService.isAuthenticated()) {
    next('/login');
  } else {
    next();
  }
});

export default router
